<?php

require_once __DIR__.'/vendor/autoload.php';

use Guzzle\Http\Client;
use RabbitMQUtils\RabbitMQUtils;
use PhpAmqpLib\Connection\AMQPConnection;

use App\Messaging\Consumer;

$http_host = 'localhost';
$http_port = '15672';
$api_url = sprintf('http://%s:%s', $http_host, $http_port);

$port = 5672;
$user = 'guest';
$pass = 'guest';
$queue = 'task_queue';

$client = new Client($api_url);
$utils = new RabbitMQUtils($client, $user, $pass);

try {
    // find a node to connect to
    $node = $utils->get_random_rabbit_node();
} catch (\Exception $e) {
    echo $e->getMessage(), "\n";
    exit(1);
}

echo "Connecting to node: ", $node, "\n";

$conn = new AMQPConnection($node, $port, $user, $pass, '/');
$ch = $conn->channel();

$callback = function($msg) {
    echo "\n--------\n";
    echo $msg->body;
    echo "\n--------\n";

    return true;
};

$consumer = new Consumer($conn, $ch, $callback);
$consumer->consume($queue);