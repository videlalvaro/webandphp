<?php

require_once __DIR__.'/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('HOST', 'localhost');
define('PORT', 5672);
define('USER', 'guest');
define('PASS', 'guest');
define('VHOST', '/');

$exchange = 'amq.direct';
$queue = 'task_queue';

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->queue_declare($queue, false, true, false, false);
$ch->queue_bind($queue, $exchange);

// exchange where to send the reply.
$ch->exchange_declare('rpc.roundtrip', 'direct', false, true, false);

function process_message($msg) {
    $reply = new AMQPMessage($msg->body, array('content_type' => 'text/plain', 'delivery_mode' => 2));
    $msg->delivery_info['channel']->basic_publish($reply, 'rpc.roundtrip');
    echo "replied\n";

    $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume($queue, '', false, false, false, false, 'process_message');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}