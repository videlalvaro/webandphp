<?php

require_once __DIR__.'/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('HOST', 'localhost');
define('PORT', 5672);
define('USER', 'guest');
define('PASS', 'guest');
define('VHOST', '/');

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$ch->exchange_declare('rpc.roundtrip', 'direct', false, true, false);
$ch->queue_declare('roundtrip', false, true, false, false);
$ch->queue_bind('roundtrip', 'rpc.roundtrip');

function microtime_float($mc) {
    list($usec, $sec) = explode(" ", $mc);
    return ((float)$usec + (float)$sec);
}

function process_message($msg) {
    echo microtime(true) - microtime_float($msg->body), "\n";
  
    $msg->delivery_info['channel']->
        basic_ack($msg->delivery_info['delivery_tag']);
}

$ch->basic_consume('roundtrip', '', false, false, false, false, 'process_message');

$msg_body = microtime();
$msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));
$ch->basic_publish($msg, 'amq.direct');

function shutdown($ch, $conn) {
    $ch->close();
    $conn->close();
}
register_shutdown_function('shutdown', $ch, $conn);

// Loop as long as the channel has callbacks registered
while (count($ch->callbacks)) {
    $ch->wait();
}