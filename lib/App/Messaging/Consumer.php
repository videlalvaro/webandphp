<?php

namespace App\Messaging;

class Consumer
{
    protected $conn;
    protected $ch;
    
    public function __construct($conn, $ch, $callback, $register_shutdown = true) {
        $this->conn = $conn;
        $this->ch = $ch;
        $this->callback = $callback;
        
        if ($register_shutdown) { // mostly for testing
            register_shutdown_function(array($this, 'shutdown'), $this->ch, $this->conn);
        }
    }
    
    public function consume($queue, $prefetch_count = 10) {
        $this->ch->basic_qos(null, $prefetch_count, null);
        $this->ch->basic_consume($queue, '', false, false, false, false, array($this, 'process_message'));
        
        // Loop as long as the channel has callbacks registered
        while (count($this->ch->callbacks)) {
            $this->ch->wait();
        }
    }

    public function process_message($msg) {
        $res = call_user_func_array($this->callback, array($msg));

        if ($res) {
            $msg->delivery_info['channel']->
                basic_ack($msg->delivery_info['delivery_tag']);
        }

        // Send a message with the string "quit" to cancel the consumer.
        if ($msg->body === 'quit') {
            $msg->delivery_info['channel']->
                basic_cancel($msg->delivery_info['consumer_tag']);
        }
    }

    public function shutdown($ch, $conn) {
        $ch->close();
        $conn->close();
    }
}