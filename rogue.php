<?php

require_once __DIR__.'/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

define('HOST', 'localhost');
define('PORT', 5672);
define('USER', 'guest');
define('PASS', 'guest');
define('VHOST', '/');

$conn = new AMQPConnection(HOST, PORT, USER, PASS, VHOST);
$ch = $conn->channel();

$msg_body = microtime();
$msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 1));

$max = $argv[1];

for ($i = 0; $i < $max; $i++) {
    $ch->basic_publish($msg, 'amq.direct');
}

$ch->close();
$conn->close();